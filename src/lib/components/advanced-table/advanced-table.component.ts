import {
  ChangeDetectorRef,
  Component,
  ElementRef, EventEmitter,
  Inject,
  InjectionToken,
  Injector,
  Input, OnDestroy,
  OnInit, Output,
  ViewChild,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';
import {TableConfiguration} from './advanced-table.interfaces';
import {DataServiceQueryParams} from '@themost/client';
import {AdvancedColumnFormatter} from './advanced-table.formatters';
import {ClientDataQueryable} from '@themost/client';
import {AdvancedFilterValueProvider} from './advanced-filter-value-provider.service';
import {Subscription, BehaviorSubject} from 'rxjs';
import {TemplatePipe, LoadingService, ConfigurationService,
  ModalService, DIALOG_BUTTONS, ErrorService, ExportSpreadsheetService} from '@universis/common';
import {IAdvancedTableFormatters} from './advanced-table.formatters.interface';
// important note: use this import to avoid Cannot call a namespace ('jquery') error
import * as jQuery_ from 'jquery';
import 'datatables.net';
import {template} from 'lodash';
import 'datatables.net-scroller';
import 'datatables.net-colreorder';
import 'datatables.net-buttons';
import { HttpClient } from '@angular/common/http';
import { ColumnSelectorService } from '../column-selector/column-selector.service';
import { ExportLimitConfiguration, MAX_EXPORT_ITEMS } from '../../directives/btnexp.directive';

// restore jquery import to avoid Cannot call a namespace ('jquery') error
const jQuery = jQuery_;
export interface AdvancedTableDataResult {
  recordsTotal: number;
  recordsFiltered: number;
  data: Array<any>;
}

const DataTablesApi: any = jQuery.fn.dataTable.Api;
DataTablesApi.register( 'processing()', function( show: any ) {
    return this.iterator('table', ( ctx: any ) => {
        ctx.oApi._fnProcessingDisplay( ctx, show );
    } );
});

const EMPTY_DATASET = {
  recordsTotal: 0,
  recordsFiltered: 0,
  data: [],
};

class TableTranslationChangeDetector extends ChangeDetectorRef {
  public checkNoChanges(): void {
  }

  public detach(): void {
  }

  public detectChanges(): void {
  }

  public markForCheck(): void {
  }

  public reattach(): void {
  }

}

export let COLUMN_FORMATTERS = new InjectionToken('column.formatters');

export class ExportLimitExceededError extends RangeError {
  constructor(message: string) {
    super(message);
    this.name = 'ExportLimitExceededError';
  }
}

class ColumnFormatter extends AdvancedColumnFormatter {
  constructor(protected injector: Injector) {
    super();
  }

  public class?: string;
  public className?: string;
  public defaultContent?: string;
  public formatString?: string;
  public formatter?: string;
  public hidden?: boolean;
  public name?: string;
  public order?: string;
  public property?: string;
  public sortable?: boolean;
  public title = '';

  public render(data: any, type: any, row: any, meta: any) {
    //
  }
}

function forceCast<T>(input: any): T {
  return input;
}

export class AdvancedTableConfiguration {
  public static cast(input: any, clone?: boolean): TableConfiguration {
    if (clone) {
      return forceCast<TableConfiguration>(JSON.parse(JSON.stringify(input)));
    }
    return forceCast<TableConfiguration>(input);
  }
}

function getRandomIdentifier() {
  return (
    Number(String(Math.random()).slice(2))
  ).toString(36);
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-advanced-table',
  template: `
    <table #table class="dataTable">
    </table>
    <div #emptyTable class="d-none">
      <div>
        <div class="mt-4">
          <div class="icon-circle bg-gray-300 border-gray-100"></div>
        </div>
        <div class="mt-4">
          <h4 class="font-3xl text-dark" [translate]="'Tables.EmptyTableTitle'"></h4>
          <p class="font-lg text-dark" [translate]="'Tables.EmptyTableMessage'"></p>
        </div>
      </div>
    </div>
    <div #errorTemplate class="d-none error-message">
      <div class="text-center">
        <div class="mt-4">
          <div class="icon-circle bg-danger border-gray-100"></div>
        </div>
        <div class="mt-4">
          <h4 class="font-3xl text-secondary" [translate]="'Tables.DataError.Title'"></h4>
          <p class="font-lg text-secondary mb-1" [translate]="'Tables.DataError.Message'"></p>
          <p class="font-lg text-secondary error-inner-message">
            <small></small>
          </p>
        </div>
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./advanced-table.component.scss'],
})
export class AdvancedTableComponent implements OnInit, OnDestroy, OnChanges {

  private _query?: ClientDataQueryable;
  private _config?: TableConfiguration;
  private _firstLoad = true;
  private _showFooter = false;
  private _showHeader = true;
  private _showPaging = false;
  private _id = getRandomIdentifier();
  public smartSelect = false;
  public configChanges = new BehaviorSubject<any>(null);

  @ViewChild('table') public table?: ElementRef;
  @ViewChild('emptyTable') public emptyTable?: ElementRef;
  @ViewChild('errorTemplate') public errorTemplate?: ElementRef;
  @ViewChild('loadingTable') public loadingTable?: ElementRef;
  @Input() public height = 800;
  @Input() public showActions = true;
  @Input() public autoLoad = true;
  @Output('load') public load: EventEmitter<AdvancedTableDataResult> = new EventEmitter<AdvancedTableDataResult>();
  @Output('init') public init: EventEmitter<any> = new EventEmitter<any>();
  @Input('reload') public reload: EventEmitter<any> = new EventEmitter<any>();
  @Output('select') public select: EventEmitter<any> = new EventEmitter<any>();
  @Output('draw') public draw: EventEmitter<any> = new EventEmitter<any>();
  @Output('page') public page: EventEmitter<any> = new EventEmitter<any>();
  @Output('order') public order: EventEmitter<any[][]> = new EventEmitter<any[][]>();
  @Output('scroll') public scroll: EventEmitter<{ top: number; left: number }> = new EventEmitter<{ top: number; left: number }>();
  @Output('selectedItems') public selectedItems: EventEmitter<any> = new EventEmitter<any>();
  @Output('loading') public loading: EventEmitter<{
    target: AdvancedTableComponent
  }> = new EventEmitter<{
    target: AdvancedTableComponent
  }>();

  @Input() public scrollable = true;
  @Input() public serverSide = true;
  @Input() public lengthMenu = [50, 100, 200, 500];
  @Input() public paging = true;
  @Input() public pageLength = 25;
  @Input() public columnOrdering = false;
  @Input() public stateSave = false;
  @Input() public stateLoadCallback?: (settings: any, callback: (state: any) => void) => void;
  @Input() public stateSaveCallback?: (settings: any, state: any) => void;

  @Input() public scrollY = 800;
  @Input() public scrollX = true;
  /**
   * Enables row selection. The defaulr value is false
   */
  @Input() public selectable: boolean | undefined = false;
  /**
   * Enables or disable multiple selection. The default values is true.
   */
  @Input() public multipleSelect: boolean | undefined = true;
  /**
   * Enables custom selection and hides select column. The default value is  false
   */
  @Input() public customSelect = false;
  @Input() public selected: any[] = [];
  public unselected: any[] = [];
  /**
   * Gets or sets a URL to load table configuration from
   */
  @Input() public configSrc?: string;
  public lastError: any;

  /**
   * Gets or sets table configuration
   */
  @Input('config')
  public set config(value: TableConfiguration) {
    this._config = value;
    // raise subscription
    this.configChanges.next(this._config);
  }

  public get config() {
    return this._config as TableConfiguration;
  }

  public get lastQueryTotalItems() {
    return this._lastQueryTotalItems;
  }

  /**
   * Indicates whether table footer will be visible or not
   */
  @Input('showFooter')
  public set showFooter(value: boolean) {
    this._showFooter = value;
    // show or hide footer
    if (this._element && this._element.nativeElement) {
      const element = ( this._element.nativeElement as HTMLDivElement)
        .querySelector('.dataTables_info');
      if (element == null) {
        return;
      }
      if (value) {
        element.classList.remove('d-none');
      } else {
        element.classList.add('d-none');
      }
    }
  }

  public get showFooter() {
    return this._showFooter;
  }

  /**
   * Indicates whether table footer will be visible or not
   */
   @Input('showHeader')
   public set showHeader(value: boolean) {
     this._showHeader = value;
     // show or hide footer
     if (this._element && this._element.nativeElement) {
       const element = ( this._element.nativeElement as HTMLDivElement)
         .querySelector('.dataTable>thead');
       if (element == null) {
         return;
       }
       const dataTable = ( this._element.nativeElement as HTMLDivElement).querySelector('.dataTable');
       if (dataTable == null) {
        return;
       }
       if (value) {
         element.classList.remove('d-none');
         // add top margin
         dataTable.classList.remove('mt-2');
       } else {
         element.classList.add('d-none');
         // remove top margin
         dataTable.classList.add('mt-2');
       }
     }
   }

   public get showHeader() {
     return this._showHeader;
   }

  /**
   * Indicates whether paging information will be visible or not
   */
  @Input('showPaging')
  public set showPaging(value: boolean) {
    this._showPaging = value;
    // show or hide footer
    if (this._element && this._element.nativeElement) {
      const element = ( this._element.nativeElement as HTMLDivElement)
        .querySelector('.dataTables_paginate');
      if (element == null) {
        return;
      }
      if (value) {
        jQuery(element).show();
      } else {
        jQuery(element).hide();
      }
    }
  }

  public get showPaging() {
    return this._showPaging;
  }

  /**
   * Gets or sets an active query for this table
   */
  @Input('query')
  public set query(value: ClientDataQueryable | undefined) {
    this._query = value;
  }

  public get query() {
    return this._query as ClientDataQueryable;
  }

  public readonly translator: TranslatePipe;
  public dataTable: any;
  public dtOptions: any;
  private lastQueryParams?: DataServiceQueryParams;
  private _lastQueryTotalItems = 0;
  private reloadSubscription: Subscription;

  constructor(private _context: AngularDataContext,
              @Inject(COLUMN_FORMATTERS) private _columnFormatters: IAdvancedTableFormatters,
              private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _injector: Injector,
              private _advancedFilterValueProvider: AdvancedFilterValueProvider,
              private _element: ElementRef,
              private _template: TemplatePipe,
              private _loading: LoadingService,
              private _http: HttpClient,
              private _columnSelector: ColumnSelectorService,
              private _configuration: ConfigurationService,
              private _exportSpreadsheet: ExportSpreadsheetService,
              private _modalService: ModalService,
              private _errorService: ErrorService) {
    // initialize translate pipe
    this.translator = new TranslatePipe(this._translateService, new TableTranslationChangeDetector());
    jQuery.fn.dataTable.ext.classes.sPageButton = 'btn';
    jQuery.fn.dataTable.ext.classes.sPageButtonActive = 'btn btn-light';
    this.reloadSubscription = this.reload.subscribe((data) => {
      this.fetch();
    });
  }

  // simple search
  public search(searchText) {
    this.query = this.query || this._context.model(this.config.model).asQueryable();
    if (this.serverSide) {
      if (searchText && this.config.searchExpression) {
        // validate search text in double quotes
        if (/^"(.*?)"$/.test(searchText)) {
          this.query.setParam('$filter',
            this._template.transform(this.config.searchExpression, {
              text: searchText.replace(/^"|"$/g, ''),
            }));
        } else {
          // try to split words
          const words = searchText.split(' ');
          // join words to a single filter
          const filter = words.map((word) => {
            return this._template.transform((this.config && this.config.searchExpression) as string, {
              text: word,
            });
          }).join(' and ');
          // set filter
          this.query.setParam('$filter', filter);
        }
      } else {
        // clear filter
        this.query.setParam('$filter', null);
      }
      this._firstLoad = false;
      this.dataTable.draw();
    } else {
      // ensure empty table layout
      this.resetEmptyTable();
      // search locally
      this.dataTable.search(searchText.trim()).draw();
      // get indexes of filtered rows
      const indexes = Array.from(this.dataTable.context[0].aiDisplay);
      // set recordsFiltered
      const recordsFiltered = indexes.length;
      // set recordsTotal
      const recordsTotal = indexes.length;
      // retrieve data from indexes
      const rows = Array.from(this.dataTable.rows().data());
      const data = indexes.map(index => rows[<number>index]);
      // emit results to load output
      this.load.emit({
        data: data,
        recordsFiltered: recordsFiltered,
        recordsTotal: recordsTotal
      });
    }
  }

  public fetch(clearSelected?: boolean) {
    // if data table is null
    if (this.dataTable == null) {
      // initialize data table
      return this.ngOnInit();
    }
    // clear selection if parameter is not declared or it's true
    if (clearSelected == null || clearSelected === true) {
      // set smart select to false
      this.smartSelect = false;
      // clear selected items
      if (this.selected.length) {
        this.selected.splice(0, this.selected.length);
      }
      // clear unselected items
      if (this.unselected.length) {
        this.unselected.splice(0, this.unselected.length);
      }
    }
    // otherwise load data only
    this.dataTable.draw();
  }

  public destroy() {
    if (this.dataTable) {
      this.dataTable.destroy();
      this._firstLoad = true;
      if (this.table) {
        jQuery(this.table.nativeElement).empty();
      }
      this.dataTable = null;
      // clear selected items
      if (this.selected.length) {
        this.selected.splice(0, this.selected.length);
      }
      // clear unselected items
      if (this.unselected.length) {
        this.unselected.splice(0, this.unselected.length);
      }
      // set smart select to false
      this.smartSelect = false;
    }
  }

  /**
   * Selects the specified data row
   */
  public selectRow(row: any, silent?: boolean) {
    if (row) {
      // get row
      const rowNode = row.node();
      // if multiple select is false
      if (this.multipleSelect === false) {
        if (this.customSelect === false) {
          // enumerate row
          this.dataTable.rows().nodes().each((node: any) => {
            if (node !== rowNode) {
              jQuery(node).find(':checkbox').prop('checked', false);
            }
          });
        } else {
          this.dataTable.rows('.selected').nodes().each((node: any) => {
            if (node !== rowNode) {
              jQuery(node).removeClass('selected');
            }
          });
        }
        // clear selected
        this.selected.splice(0, this.selected.length);
      }
      // if custom select mode is off
      if (this.customSelect === false) {
        jQuery(rowNode).find(':checkbox').prop('checked', true);
      } else {
        // otherwise add class
        jQuery(row.node()).addClass('selected');
      }
      // get row data
      const key = this.config.columns[0].property || this.config.columns[0].name || '';
      const data = row.data();
      // search selected items
      let findIndex = this.selected.findIndex((x) => {
        return x[key] === data[key];
      });
      if (findIndex >= 0) {
        // remove previously selected row
        this.selected.splice(findIndex, 1);
      }
      this.selected.push(data);
      // validate unselected data
      findIndex = this.unselected.findIndex((x) => {
        return x[key] === data[key];
      });
      if (findIndex >= 0) {
        // remove previously selected row
        this.unselected.splice(findIndex, 1);
      }
      if (silent) {
        return;
      }
      this.selectedItems.emit(this.selected);
    }
  }

  /**
   * Unselects the specified data row if it's selected
   */
  public unSelectRow(row: any, silent?: boolean) {
    if (row) {
      // if custom select mode is off
      if (this.customSelect === false) {
        jQuery(row.node()).find(':checkbox').prop('checked', false);
      } else {
        // otherwise add class
        jQuery(row.node()).removeClass('selected');
      }
      // get first column key
      const key = this.config.columns[0].property || this.config.columns[0].name || '';
      const data = row.data();
      const id = data[key];
      let findIndex = this.selected.findIndex((x) => {
        return x[key] === id;
      });
      if (findIndex >= 0) {
        this.selected.splice(findIndex, 1);
        if (this.smartSelect) {
          // add this row to unselected rows
          findIndex = this.unselected.findIndex((x) => {
            return x[key] === id;
          });
          if (findIndex < 0) {
            this.unselected.push(data);
          }
        }
        if (silent) {
          return;
        }
        this.selectedItems.emit(this.selected);
      }
    }
  }

  public selectAny() {
    if (this.dataTable && this.selectable) {
      // set smart select flag
      this.smartSelect = true;
      // select any row
      this.dataTable.rows().every((index: any) => {
        const row = this.dataTable.row( index );
        this.selectRow(row, true);
      });
      // emit selection event
      this.selectedItems.emit(this.selected);
    }
  }

  public selectNone() {
    if (this.dataTable && this.selectable) {
      // set smart select flag
      this.smartSelect = false;
      this.unselected.splice(0, this.unselected.length);
      // select any row
      this.dataTable.rows().every((index: any) => {
        const row = this.dataTable.row( index );
        this.unSelectRow(row, true);
      });
      this.selected.splice(0, this.selected.length);
      // emit selection event
      this.selectedItems.emit(this.selected);
    }
  }

  public toggleSelectRow(row: any) {
    if (row) {
      const data = row.data();
      // get first column key
      const key = this.config.columns[0].property || this.config.columns[0].name || '';
      // get id
      const id = data[key];
      // find if row is selected
      const findIndex = this.selected.find((x) => {
        return x[key] === id;
      });
      // if row is selected
      if (findIndex >= 0) {
        // unselect
        this.unSelectRow(row);
      } else {
        this.selectRow(row);
      }
    }
  }

  // noinspection JSMethodCanBeStatic
  /**
   * Datatables column renderer for select checkbox
   * @param data
   * @param type
   * @param row
   * @param meta
   */
  private _renderSelect(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (typeof column.selectTemplate !== 'function') {
      Object.assign(column, {
        selectTemplate: template(`<input class="checkbox checkbox-theme s-row" type="checkbox"
        id="s-${this._id}-\${meta.row}" data-id="\${data}" /><label for="s-${this._id}-\${meta.row}">&nbsp;</label>
        `),
      });
    }
    // render template
    return column.selectTemplate({
      meta,
      data,
    });
  }

  public ngOnInit() {
    const self = this;
    if (this.dataTable) {
      this.destroy();
    }
    if (this.config) {
      this.loading.emit({
        target: this
      });
      // parse properties
      if (Object.prototype.hasOwnProperty.call(this.config, 'selectable')) {
        this.selectable = this.config.selectable;
      }
      if (Object.prototype.hasOwnProperty.call(this.config, 'multipleSelect')) {
        this.multipleSelect = this.config.multipleSelect;
      }
      // prepare configuration for columns
      const tableColumns = this.config.columns
        // filter action link column
        .filter((column) => {
          if (column.formatter && column.formatter === 'ActionLinkFormatter' && this.showActions === false) {
            return false;
          }
          return true;
        }).map((column) => {
          // convert column to advance table column
          return Object.assign(new ColumnFormatter(this._injector), column);
        }).map((column) => {
          const columnDefinition = {
            data: column.property || column.name,
            defaultContent: column.defaultContent != null ? column.defaultContent : '',
            title: this.translator.transform(column.title),
            sortable: column.hasOwnProperty('sortable') ? column.sortable : true,
            visible: column.hasOwnProperty('hidden') ? !column.hidden : true,
            name: column.name,
          };

          if (column.className) {
            Object.assign(columnDefinition, {
              className: column.className,
            });
          } else {
            Object.assign(columnDefinition, {
              className: '',
            });
          }

          if (column.formatters) {
            Object.assign(columnDefinition, {
              render(data: any, type: string, row: any, meta: any) {
                let result = data;
                (column.formatters as any[]).forEach((columnFormatter) => {
                  const formatter = self._columnFormatters[columnFormatter.formatter];
                  if (formatter && typeof formatter.render === 'function') {
                    column.formatString = columnFormatter.formatString;
                    column.formatOptions = columnFormatter.formatOptions;
                  }
                  result = formatter.render.bind(column)(result, type, row, meta);
                });
                delete column.formatString;
                delete column.formatOptions;
                return result;
              },
            });
            // check if column formatters export createdCell event
            const items = column.formatters.filter((columnFormatter) => {
                return typeof columnFormatter.afterRender === 'function';
            });
            if (items.length > 0) {
              Object.assign(columnDefinition, {
                createdCell(cell: any, cellData: any, rowData: any, rowIndex: number, colIndex: number) {
                  items.forEach((columnFormatter) => {
                    const formatter = self._columnFormatters[columnFormatter.formatter];
                    if (formatter && typeof formatter.afterRender === 'function') {
                      formatter.afterRender.bind(column)(cell, cellData, rowData, rowIndex, colIndex);
                    }
                  });
                },
              });
            }
          }
          if (column.formatter) {
            const formatter = self._columnFormatters[column.formatter];
            if (formatter && typeof formatter.render === 'function') {
              Object.assign(columnDefinition, {
                render: formatter.render.bind(column),
              });
            }
            if (formatter && typeof formatter.afterRender === 'function') {
              Object.assign(columnDefinition, {
                render: formatter.render.bind(column),
              });
              Object.assign(columnDefinition, {
                createdCell: formatter.afterRender.bind(column),
              });
            }
          }
          return columnDefinition;
        });
      // get table element
      const tableElement = jQuery(this.table && this.table.nativeElement);
      jQuery(window).on('resize', () => {
        setTimeout(() => {
          if (this.dataTable) {
            this.dataTable.columns.adjust();
          }
        }, 1);
      });
      // tslint:disable-next-line: no-shadowed-variable
      tableElement.on('processing.dt', (event, settings, processing) => {
        if (processing) {
          // clear empty table element
          tableElement.find('.dataTables_empty').html('');
          setTimeout(() => {
            tableElement.addClass('processing');
          }, 0);
        } else {
          setTimeout(() => {
            tableElement.removeClass('processing');
          }, 0);
        }
      });
      let drawIndex = 1;
      tableElement.on('order.dt', (event: any) => {
        if (this.dataTable) {
          const order = this.dataTable.order();
          const columns = tableColumns;
          if (order && order.length) {
            this.order.emit(order);
          }
        }
      }).on('draw.dt', (event) => {
        this.draw.emit();
        if (this.dataTable) {
          const info: { page: number; pages: number } = this.dataTable.page.info();
          this.page.emit(info);
          if (drawIndex > 1) {
            return;
          }
          try {
            drawIndex++;
            setTimeout(() => {this.dataTable.columns.adjust(); }, 1);
          } catch (err) {
            console.error('ERROR', 'An error occurred while adjusting table columns');
            console.error(err);
          }
        }
      });

      // allow selection
      if (this.selectable) {
        // get first column (as row primary key)
        // todo::allow row primary key configuration
        const firstColumn = this.config.columns[0];
        // add select column
        const selectColumnDefinition = {
          className: 'text-center pl-4 pr-0',
          data: firstColumn.property || firstColumn.name,
          defaultContent: '',
          title: '',
          sortable: false,
          visible: !this.customSelect,
          name: firstColumn.name,
        };
        // assign renderer
        Object.assign(selectColumnDefinition, {
          render: this._renderSelect.bind(this),
        });
        // and finally add column
        tableColumns.unshift(selectColumnDefinition);
        // user interface additions
        // handle page change event (a trick for firing draw.dt events)
        tableElement
          // handle draw events
          .on('draw.dt', () => {

            if (this.customSelect) {
              // enable row selection event
              tableElement.find('tbody').on('click', 'tr', function() {
                // select row
                const row = self.dataTable.row(this);
                // add row
                self.select.emit(row);
              });
            }
            const elements = tableElement.find('input.s-row');
            elements.off('click').on('click', this.onSelectionChange.bind(this));
            // make selection on current page
            const key = this.config.columns[0].property || '';
            if (this.selected.length > 0) {
              // enumerate selected items
              this.selected.forEach((item) => {
                // find row identifier
                const id = item[key];
                // if identifier is defined
                if (id != null) {
                  // try to find check element with this identifier in attribute data-id
                  tableElement.find(`input[data-id='${id}']`).prop('checked', true);
                }
              });
            }
            if (this.smartSelect) {
              // select rows that are not selected
              // and do not included in unselected collection
              tableElement.find('input[data-id]').each((index, element) => {
                // get data id
                if (this.unselected.findIndex((x) => {
                  // tslint:disable-next-line:triple-equals
                  return x[key] == jQuery(element).attr('data-id');
                }) < 0) {
                  this.selectRow(this.dataTable.row(index));
                }
              });
            } else if (this.selected.length === 0) {
              tableElement.find('input[data-id]').prop('checked', false);
            }
          });
      }
      // initialize data table
      const settings = {
        // set length menu
        lengthMenu: [5, 10, 50, 100, 200, 500],
        // hide length menu
        lengthChange: false,
        // enable search but hide search box (css)
        searching: true,
        // enable data processing
        processing: true,
        // enable getting server side data
        serverSide: this.serverSide,
        // enumerate table columns
        columns: tableColumns,
        colReorder:  this.columnOrdering,
        order: [],
        // set scroll x
        scrollX: this.scrollX,
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
          {
            extend: 'excel',
            filename: this.config.title,
            className: 'd-none',
            exportOptions: {
              columns: 'th:not(:first-child)',
            },
          },
        ],
        // define server data callback
        fnServerData(sSource: any, aoData: any, fnCallback: any, oSettings: any) {
          // if method is invoked for the first time
          if (self._firstLoad) {
            // disabled first load flag
            self._firstLoad = false;
            // if auto load is disabled
            if (self.autoLoad === false) {
              const emptyDataSet = {
                recordsTotal: 0,
                recordsFiltered: 0,
                data: [],
              };
              self._lastQueryTotalItems = 0;
              // emit empty data
              self.load.emit(emptyDataSet);
              // return empty data
              return fnCallback(emptyDataSet);
            }
          }
          // reset error
          self.toggleError();
          // get activated route params
          const queryParams = self._activatedRoute.snapshot.queryParams;
          // get columns
          const columns = aoData[1].value;
          // get order expression
          const order = aoData[2].value;
          // get skip records param
          const skip = aoData[3].value;
          // get page size param
          const top = aoData[4].value;
          // get search value
          const search = aoData[5].value;
          // get data queryable
          const q = self._query instanceof ClientDataQueryable ? self._query : self._context.model(self.config.model).asQueryable();
          // apply paging params
          if (top) {
            q.take(top).skip(skip);
          }
          // apply order params
          if (order && order.length) {
            // get order query expression
            const orderByStr = order.map((expr: any) => {
              let columnIndex: number;
              if (self.columnOrdering === false) {
                columnIndex = expr.column;
              } else {
                // get the current column order (from colReorder)
                const columnOrder: number[] = self.dataTable.colReorder.order();
                // and match the dataTable index to the config columns index
                // expr.column should always be valid (e.g. in range), but perform a minor validation
                if (expr.column >= 0 && expr.column < columnOrder.length) {
                  columnIndex = columnOrder[expr.column];
                } else {
                  // use the first column as a fallback (won't happen, just for safety)
                  columnIndex = 0;
                }
              }
              return tableColumns[columnIndex].name + ' ' + expr.dir || 'asc';
            }).join(',');
            // set order
            q.setParam('$orderby', orderByStr);
          }
          // ensure self.stateLoadCallback
          if (self.stateLoadCallback == null) {
            self.stateLoadCallback = ((_, callback)  => {
              return callback(null);
            });
          }
          self.stateLoadCallback(null, (state) => {
            // configure $select parameter
            const select = self.config.columns.filter( (column) => {
              if (column.virtual === true) {
                return false;
              }
              if (column.optional === true) {
                if (state && state.columns) {
                  const index = tableColumns.findIndex((tableColumn) => tableColumn.name === column.name);
                  if (index >= 0) {
                    return state.columns[index].visible;
                  }
                }
                return false;
              }
              return true;
            }).map((column) => {
              if (column.property) {
                return column.name + ' as ' + column.property;
              } else {
                return column.name;
              }
            });
            q.select.apply(q, select);
            // configure $filter
            if (queryParams.$filter && queryParams.$filter.length) {
              // set route filter
              const qParams = q.getParams();
              q.setParam('$filter', null);
              const expressions = [ queryParams.$filter ];
              if (qParams.$filter) { expressions.push('(' + qParams.$filter + ')' ); }
              q.filter(expressions.join(' and '));
            }
            // check if query parameters contain filter
            if (queryParams.$expand && queryParams.$expand.length) {
              // set route $expand
              q.setParam('$expand', queryParams.$expand);
            } else if (self.config && self.config.defaults && self.config.defaults.expand) {
              q.setParam('$expand', self.config.defaults.expand);
            }
            if (!q.getParams()['$orderby'] && self.config && self.config.defaults && self.config.defaults.orderBy) {
              q.setParam('$orderby', self.config.defaults.orderBy);
            }
            if (self.config && self.config.defaults && self.config.defaults.groupBy) {
              q.setParam('$groupby', self.config.defaults.groupBy);
            }
            // append default filter
            if (self.config && self.config.defaults && self.config.defaults.filter) {
              return self._advancedFilterValueProvider.asyncBuildFilter(self.config.defaults.filter).then((defaultFilterString) => {
                if (defaultFilterString) {
                  const qParams = q.getParams();
                  q.setParam('$filter', null);
                  const expressions = [ defaultFilterString ];
                  if (qParams.$filter) { expressions.push('(' + qParams.$filter + ')' ); }
                  q.filter(expressions.join(' and '));
                }
                self.lastQueryParams = q.getParams();
                return q.getList().then((items) => {
                    const dataSet = {
                      recordsTotal: items.total,
                      recordsFiltered: items.total,
                      data: items.value,
                    };
                    self._lastQueryTotalItems = items.total;
                    if (items.total === 0) {
                      self.resetEmptyTable();
                    }
                    // emit load event
                    self.load.emit(dataSet);
                    // return data
                    return fnCallback(dataSet);
                });
              }).catch((err) => {
                // log error to console
                console.error('TABLES', err);
                // show error
                self.toggleError(err);
                self._lastQueryTotalItems = 0;
                // by returning empty dataset
                self.load.emit(EMPTY_DATASET);
                return fnCallback(EMPTY_DATASET);
              });
            }
            self.lastQueryParams = q.getParams();
            q.getList().then((items) => {
              const dataSet = {
                recordsTotal: items.total,
                recordsFiltered: items.total,
                data: items.value,
              };
              self._lastQueryTotalItems = items.total;
              if (items.total === 0) {
                self.resetEmptyTable();
              }
              // emit load event
              self.load.emit(dataSet);
              // return data
              return fnCallback(dataSet);
            }).catch((err) => {
              // log error to console
              console.error('TABLES', err);
              // show error
              self.toggleError(err);
              // by returning empty dataset
              self.load.emit(EMPTY_DATASET);
              return fnCallback(EMPTY_DATASET);
            });
          });
        },
        language: Object.assign({ }, self.translator.transform('Tables.DataTable'),
          {emptyTable: ' ' }),
      };

      const stateSettings = {
        stateSave: this.stateSave
      };
      if (this.stateLoadCallback) {
        Object.assign(stateSettings, {
          stateLoadCallback: this.stateLoadCallback
        });
      }
      if (this.stateSaveCallback) {
        Object.assign(stateSettings, {
          stateSaveCallback: this.stateSaveCallback
        });
      }
      Object.assign(settings, stateSettings);

      if (this.scrollable) {
        // set continuous scrolling
        Object.assign(settings, {
          scrollY: this.scrollY,
          scrollCollapse: false,
          scroller: {
            loadingIndicator: false,
            displayBuffer: 10,
          },
        });
      } else {
        Object.assign(settings, {
          scrollY: this.scrollY,
          paging: this.paging,
          pageLength: this.pageLength,
        });
      }
      if (this.selectable) {

      }
      // handle init.dt event and emit local event
      this.dataTable = tableElement.on('init.dt', (event) => {
        const scroller = jQuery(event.target).parent();
        // handle scrolling
        scroller.on('scroll', (scrollEvent) => {
          const target = jQuery(scrollEvent.target);
          const top = target.scrollTop() as number;
          const left = target.scrollLeft() as number;
          if (this.dataTable) {
            this.scroll.emit({
              top,
              left
            });
          }
        });
      }).DataTable(settings);
      this.init.emit();
      // show or hide header
      if (!this.showHeader && this._element && this._element.nativeElement) {
        const hideSelector = (this._element.nativeElement as HTMLDivElement)
          .querySelector('.dataTable>thead');
        if (hideSelector) {
          hideSelector.classList.add('d-none');
        }
        const adjustSelector =  (this._element.nativeElement as HTMLDivElement)
          .querySelector('.dataTable');
        if (adjustSelector) {
          adjustSelector.classList.add('mt-2');
        }
      }
      // show or hide footer
      if (!this.showFooter && this._element && this._element.nativeElement) {
        const infoSelector = (this._element.nativeElement as HTMLDivElement)
          .querySelector('.dataTables_info');
        if (infoSelector) {
          infoSelector.classList.add('d-none');
        }
      }
      // show or hide pager
      if (this.showPaging) {
        this.showPaging = true;
      }
    }
  }

  scrollTop(value: number) {
    if (this.dataTable) {
      this.dataTable.parent().scrollTop(value);
    }
  }

  scrollLeft(value: number) {
    if (this.dataTable) {
      this.dataTable.parent().scrollLeft(value);
    }
  }

  get lastQuery(): ClientDataQueryable | undefined {
    if (this.config == null) {
      return;
    }
    if (this.lastQueryParams == null) {
      return;
    }
    const q = this._context.model(this.config.model).asQueryable();
    Object.keys(this.lastQueryParams).forEach( (key) => {
      q.setParam(key, this.lastQueryParams && this.lastQueryParams[key]);
    });
    // return query
    return q;
  }

  /**
   * Fetches and updates a single row
   * @param find - Any object which is being converted to a query
   */
  public fetchOne(find: any) {
    if (find == null) {
      return;
    }
    if (this.dataTable == null) {
     return;
    }
    if (this.lastQueryParams == null) {
      return;
    }
    // get rows
    const rows = Array.from(this.dataTable.rows().data());
    if (rows && rows.length) {
      // find result
      const findKeys = Object.keys(find);
      if (findKeys.length === 0) {
        // do nothing
        return;
      }
      // find row index by searching each property
      const rowIndex = rows.findIndex((row: any) => {
        return findKeys.map( (key) => {
          return row[key] === find[key];
        }).filter( (res) => {
          return res === false;
        }).length === 0;
      });
      // if row has been found
      if (rowIndex >= 0) {
        const query = this._context.model(this.config.model).asQueryable();
        // select attributes
        query.setParam('$select', this.lastQueryParams.$select);
        if (this.lastQueryParams.$expand) {
          // apply expand attributes
          query.setParam('$expand', this.lastQueryParams.$expand);
        }
        // apply filter for row
        findKeys.forEach( (key) => {
          const column = this.config.columns.find( (col) => {
            return col.property === key || col.name === key;
          });
          if (column) {
            // get property or name
            const property = column.property || column.name;
            query.and(property as string).equal(find[key]);
          } else {
            query.and(key).equal(find[key]);
          }
        });
        // and finally get item
        return query.getItem().then( (item) => {
          // if item exists
          const row = this.dataTable.row(rowIndex);
          const checked = jQuery(row.node()).find(':checkbox').prop('checked');
          if (item) {
            // redraw row
            row.data(item);
            jQuery(row.node()).find(':checkbox').off('click').on('click', this.onSelectionChange.bind(this));
            // if row is already selected
            if (checked) {
              this.selectRow(row, true);
            }
          }
          return Promise.resolve();
        });
      }
    }
    return Promise.resolve();
  }

  public ngOnDestroy(): void {
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
  }

  /**
   * Handle selection change
   * @param $event
   */
  public onSelectionChange($event) {
    const input =  $event.target as HTMLInputElement;
    const execRegex = /-(\d+)$/g.exec(input.id);
    // get row from id
    const rowIndex = parseInt((execRegex && execRegex[1]) as string, 10);
    // get row
    const row = this.dataTable.row(rowIndex).data();
    // get row identifier
    if (row) {
      const key = this.config.columns[0].property || '';
      const id = row[key];
      let findItemIndex = this.selected.findIndex((item) => {
        return item[key] === id;
      });
      if (input.checked) {
        if (findItemIndex < 0) {
          // add row to selected rows
          this.selected.push(row);
          this.selectedItems.emit(this.selected);
        }
      } else {
        if (findItemIndex >= 0) {
          // remove selected item
          this.selected.splice(findItemIndex, 1);
          if (this.smartSelect) {
            // add row to unselected rows
            findItemIndex = this.unselected.findIndex((item) => {
              return item[key] === id;
            });
            if (findItemIndex < 0) {
              this.unselected.push(row);
              this.selectedItems.emit(this.selected);
            }
          }
        }
      }
    }
  }

  private resetEmptyTable() {
    // this operation is important
    // due to ng data binding
    // empty table template has no translated text during ngOnInit()
    if (this.dataTable && this.dataTable.context[0]) {
      this.dataTable.context[0].oLanguage.sEmptyTable =
      this.dataTable.context[0].oLanguage.sZeroRecords = this.emptyTable && this.emptyTable.nativeElement.innerHTML;
    }
  }

  private toggleError(err?: any) {
    if (this.dataTable && this.dataTable.context[0]) {
      const element = (this.errorTemplate && this.errorTemplate.nativeElement as HTMLDivElement);
      let innerMessage: any;
      if (element) {
        innerMessage = element.getElementsByTagName('small').item(0);
      }
      if (err) {
        this.lastError = err;
        if (innerMessage) {
          innerMessage.innerHTML = err.error && err.error.message ? err.error.message : err.message;
        }
        this.dataTable.context[0].oLanguage.sEmptyTable =
        this.dataTable.context[0].oLanguage.sZeroRecords = this.errorTemplate && this.errorTemplate.nativeElement.innerHTML;
      } else {
        if (innerMessage) {
          innerMessage.innerHTML = '';
        }
        this.dataTable.context[0].oLanguage.sEmptyTable = this.dataTable.context[0].oLanguage.sZeroRecords = '';
      }
    }
  }

  private clearEmptyTable() {
    if (this.dataTable && this.dataTable.context[0]) {
      this.dataTable.context[0].oLanguage.sEmptyTable = this.dataTable.context[0].oLanguage.sZeroRecords = '';
    }
  }

  public export() {
    const element: HTMLElement = document.querySelector('.dt-buttons .buttons-excel') as HTMLElement;
    element.click();
  }

  public handleColumns(columnsToShow) {
    const table = this.dataTable;
    this.config.columns.slice(1).forEach((column, i) => {
      if (!column.hidden) {
        if (columnsToShow.includes(column.name)) {
          // noinspection TypeScriptValidateJSTypes
          table.column(column.name + ':name').visible(true);
        } else {
          // noinspection TypeScriptValidateJSTypes
          table.column(column.name + ':name').visible(false);
        }
      }
    });
  }

  public getConfig() {
    return this.config;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (Object.prototype.hasOwnProperty.call(changes, 'configSrc')) {
      if (changes.configSrc != null) {
        // load table configuration on change
        // tslint:disable-next-line: triple-equals
        if (changes.configSrc.currentValue && (changes.configSrc.currentValue != changes.configSrc.previousValue)) {
          this._http.get(changes.configSrc.currentValue).toPromise().then((config: TableConfiguration) => {
            // set configuration
            this.config = config;
            this.destroy();
            // fetch and redraw
            this.fetch();
          }).catch((err) => {
            console.error('TABLES', 'An error occurred while loading table configuration.');
            console.error(err);
          });
        }
      }
    }
  }

  public reset(load?: boolean): void {
    this.query = undefined;
    this.destroy();
    this.autoLoad = !!load;
    this.ngOnInit();
  }

  selectColumns() {
    // get column order e.g. [0, 4, 1, 2, 3]
    const columnOrder: number[] = this.dataTable.colReorder.order();
    // get column definitions
    const columnDefs = this.dataTable.settings().init().columns;
    const columns = columnDefs.map((
      columnDef: { key: string, title: string | undefined, visible: boolean,
        sortable: boolean, index: number }, i: number) => {
      const key = `column-${i}`;
      let title = columnDef.title;
      // get column index
      const index =  columnOrder.indexOf(i);
      const visible = this.dataTable.column(index).visible();
      // set title for select column
      if (i === 0 && this.selectable) {
        title = '<' + this._translateService.instant('Tables.SelectColumn') + '>';
      }
      // get column title
      if (title != null && title.length === 0) {
        title = undefined;
      }
      if (title == null) {
        // try to find column placeholder
        let placeholder: string | undefined;
        if (this.selectable) {
          if (i > 0) {
            placeholder = this.config.columns[i - 1].placeholder;
          }
        } else {
          placeholder = this.config.columns[i].placeholder;
        }
        if (placeholder != null) {
          title = '<' + this._translateService.instant(placeholder) + '>';
        }
      }
      if (title == null) {
        title = '<' + this._translateService.instant('Tables.UnknownColumn') + '>';
      }
      return {
        key,
        title,
        visible,
        index
      };
    });
    this._columnSelector.selectColumns(columns).then((event) => {
      if (event.result === 'ok') {
        const sorted = (event.columns as any[])
          .map((col) => {
            return {
              key: col.key,
              index: col.index
            };
          })
          .sort((a, b) => a.index - b.index);
        // get column order e.g. [3, 1, 2, 0, 4]
        const indexes = sorted
        .map((item) => {
          return (event.columns as any[]).findIndex((next) => item.key === next.key);
        });
        // reset column visibilty
        for (let index = 0; index < (event.columns as any[]).length; index++) {
          const element = (event.columns as any[])[index];
          const colIndex = columnOrder.indexOf(index);
          // set visibilty with redraw (=true)
          this.dataTable.column(colIndex).visible(element.visible, true);
        }
        // finally apply column order
        this.dataTable.colReorder.order(indexes, true);
        // simply redraw the table
        this.dataTable.draw();
      }
    });
  }

  /**
   *
   * @param options to pass an errorHandler callback, if desired
   * @returns void
   */
  public async exportView(options?: {errorHandler?: (reason?: any) => any}): Promise<void> {
    try {
      this._loading.showLoading();
      // validate query items and config
      if (!(this.lastQuery && this._lastQueryTotalItems && this.config)) {
        // do nothing
        return;
      }
      // get export settings
      const exportSettings = this._configuration.settings as ExportLimitConfiguration;
      // and export limit
      const exportLimit =
        (exportSettings && exportSettings.tables && exportSettings.tables.exportLimit && exportSettings.tables.exportLimit > 0)
        ? exportSettings.tables.exportLimit : MAX_EXPORT_ITEMS;
      // and validate it
      if (this._lastQueryTotalItems > exportLimit) {
        // call error handler
        // provide a recognizible error type
        if (options && options.errorHandler) {
          options.errorHandler(new ExportLimitExceededError('Export limit exceeded'));
          return;
        }
        // or show warning modal and exit
        this._modalService.showWarningDialog(this._translateService.instant('Tables.ExportList'),
          this._translateService.instant('Tables.ExportLimitExceeded',
            {dataLength: this._lastQueryTotalItems, exportLimit}), DIALOG_BUTTONS.Ok);
        return;
      }
      // get total displayed items
      const totalDisplayedItems = Array.from(this.dataTable.context[0].aiDisplay).length;
      // if they already satisfy the last query total
      if (totalDisplayedItems === this._lastQueryTotalItems) {
        // prepare excel data immediately
        const {body, header} = this.getExportableData();
        // and export them to excel
        this._exportSpreadsheet.export(body, `${this._translateService.instant(this.config.title || 'Tables.ExportResults')}.xlsx`, {
            header,
            skipHeader: false
        });
      } else {
        // at this point, more data need to be fetched
        // hold previous data state
        const previousDataState = this.dataTable.rows().data().toArray();
        // get target data state by executing the last query
        // note: taking the exportLimit amount of items will always satisfy the results,
        // because it has been validated
        const targetDataState = await this.lastQuery.take(exportLimit).getItems();
        // change table data on the fly, but do not redraw
        this.dataTable.clear().rows.add(targetDataState);
        // prepare excel data
        const {body, header} = this.getExportableData();
        // reset table data
        this.dataTable.clear().rows.add(previousDataState);
        // and export target data to excel
        this._exportSpreadsheet.export(body, `${this._translateService.instant(this.config.title || 'Tables.ExportResults')}.xlsx`, {
            header,
            skipHeader: false
        });
      }
      // exit
      return;
    } catch (err) {
      // log error
      console.error(err);
      // call error handler, if defined
      if (options && options.errorHandler) {
        options.errorHandler(err);
      } else {
        // use default error service
        this._errorService.showError(err, {
          continueLink: '.'
        });
      }
    } finally {
      this._loading.hideLoading();
    }
  }

  public getExportableData(): {body: any[], header: any[]} {
    if (!(this.dataTable && this.dataTable.buttons
      && typeof this.dataTable.buttons.exportData === 'function' && this.config)) {
      return {body: [], header: []};
    }
    // fetch exportable excel data
    let {body, header} = this.dataTable.buttons.exportData();
    const cleanupIndexes: number[] = [];
    // clean up headers
    header = header.filter((headerItem: string, index: number) => {
      // exclude empty headers
      if (headerItem.trim().length === 0) {
        cleanupIndexes.push(index);
        return false;
      }
      // exclude hidden columns
      // note: use the datatable visible() function
      // instead of the hidden config property, to satisfy the column selector
      if (!this.dataTable.column(index).visible()) {
        cleanupIndexes.push(index);
        return false;
      }
      return true;
    });
    // clean up body
    body = body.map((row: any []) => {
      return row.filter((_, rowIndex: number) => {
        return cleanupIndexes.indexOf(rowIndex) === -1;
      });
    });
    return {body, header};
  }
}
