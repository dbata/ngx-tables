import { Injectable } from '@angular/core';
import { ModalService } from '@universis/common';
import { AdvancedSelectComponent } from './advanced-select.component';
@Injectable()
export class AdvancedSelectService {

  constructor(private _modal: ModalService) {
    //
  }

  select(initialState: any): Promise<{result: string, items: Array<any>}> {
    return new Promise((resolve, reject) => {
      try {
        const modalRef = this._modal.openModalComponent(AdvancedSelectComponent, {
          class: 'modal-xl modal-table',
          ignoreBackdropClick: true,
          animated: true,
          initialState: initialState
        });
        const componentRef: AdvancedSelectComponent = modalRef.content;
        componentRef.dismiss.subscribe((result) => {
          return resolve({
            result: result,
            items: (componentRef.advancedTable && componentRef.advancedTable.selected) as any[]
          });
        });
      } catch (err) {
        return reject(err);
      }
    });
  }
}
