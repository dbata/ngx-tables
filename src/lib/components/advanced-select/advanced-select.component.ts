import { AdvancedTableModalBaseComponent } from '../advanced-table-modal/advanced-table-modal-base.component';
import { ActivatedRoute, Router } from '@angular/router';
import { AdvancedFilterValueProvider } from '../advanced-table/advanced-filter-value-provider.service';
import { AngularDataContext } from '@themost/angular';
import { AfterViewInit, Component, ElementRef, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DatePipe } from '@angular/common';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-advanced-select',
  templateUrl: './advanced-select.component.html',
  styles: [
    `
    .modal-table .dataTables_wrapper {
      height: 50vh;
      overflow-y: auto;
    }
    `,
  ],
  encapsulation: ViewEncapsulation.None,
})
export class AdvancedSelectComponent extends AdvancedTableModalBaseComponent implements AfterViewInit {

  public dismiss: EventEmitter<any> = new EventEmitter();
  @ViewChild('searchElement') searchElement: any;

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    context: AngularDataContext,
    advancedFilterValueProvider: AdvancedFilterValueProvider,
    datePipe: DatePipe,
    private _modalRef: BsModalRef,
    private el: ElementRef) {
    super(router, activatedRoute, context, advancedFilterValueProvider, datePipe);
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    setTimeout(() => {
      if (this.searchElement && this.searchElement.nativeElement) {
        this.searchElement.nativeElement.focus();
      }
    }, 0);
  }

  public async ok() {
    this._modalRef.hide();
    this.dismiss.emit('ok');
  }

  public async cancel() {
    this._modalRef.hide();
    this.dismiss.emit('cancel');
  }

}
