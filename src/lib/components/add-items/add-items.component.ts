import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {AdvancedTableModalBaseComponent} from '../advanced-table-modal/advanced-table-modal-base.component';
import {AdvancedFormComponent} from '@universis/forms';
import {LoadingService, ModalService} from '@universis/common';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-add-items',
  templateUrl: './add-items.component.html'
})
export class AddItemsComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  public loading = false;
  public lastError;
  @Input() execute?: Observable<any>;
  @Input() items: Array<any> = [];
  @Input() target: any;
  // @Input() selected: any;
  @Input() tableConfiguration: any;
  @Input() formName: any;
  @Input() messages: Array<any> = [];
  @ViewChild('formComponent') formComponent?: AdvancedFormComponent;
  @ViewChild('selectComponent') selectComponent?: AdvancedTableModalBaseComponent;
  private formLoadSubscription?: Subscription;
  private formChangeSubscription?: Subscription;
  private selectedSubscription?: Subscription;

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _modalService: ModalService,
              private _loadingService: LoadingService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }

  ngOnInit() {
    if (this.selectComponent && this.selectComponent.advancedTable) {
      this.selectedSubscription = this.selectComponent.advancedTable.selectedItems.subscribe((items) => {
        if (this.formName == null) {
          this.okButtonDisabled = items.length === 0;
        }
      });
    }
    if (this.formName != null && this.formComponent) {
      this.formChangeSubscription = this.formComponent.form.change.subscribe((event: any) => {
        if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
          // enable or disable button based on form status
          this.okButtonDisabled = (!event.isValid);
        }
      });
    }

    if (this.target) {
      const target = this.target;
    }
  }

  cancel(): Promise<any> {
    this.clearMessages();
    if (this.loading) {
      return Promise.resolve();
    }
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
    return Promise.resolve();
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
    if (this.selectedSubscription) {
      this.selectedSubscription.unsubscribe();
    }
  }

    // get form data
  private executeSubscription?: Subscription;
    public get formData(): any {
      if (this.formComponent === null) {
        return null;
      }
      return this.formComponent && this.formComponent.form.formio.data;
    }

    ok(): Promise<any> {
      try {
        this.clearMessages();
        return new Promise((resolve) => {
          if (this.selectComponent == null || this.selectComponent.advancedTable == null || this.execute == null) {
            return resolve(null);
          }
          this.loading = false;
          this.lastError = null;
          // set courses
          this.items = this.selectComponent.advancedTable.selected;
          // execute add
          this.executeSubscription = this.execute.subscribe((result) => {
            this.loading = false;
            if (this.executeSubscription) {
              this.executeSubscription.unsubscribe();
            }
            // close modal
            if (this._modalService.modalRef) {
              this._modalService.modalRef.hide();
            }
            return resolve(null);
          }, (err) => {
            this.loading = false;
            // ensure that loading is hidden
            this._loadingService.hideLoading();
            // set last error
            this.lastError = err;
            return resolve(null);
          });
       });
    } catch (err) {
      this.loading = false;
      this._loadingService.hideLoading();
      this.lastError = err;
      return Promise.resolve();
    }
  }

  onChange($event: any) {}

  clearMessages() {
    this.messages = [];
  }

  onFilterChange(event:  { name: string, emit: boolean }) {}
}
