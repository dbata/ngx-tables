import {
    Directive, Optional, Input, OnInit, OnDestroy
  } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppendFilter } from '../actions/table.actions';
import { AdvancedTableSearchComponent } from '../components/advanced-table/advanced-table-search.component';
import { NgxTablesState, selectTable } from '../reducers';
import { first } from 'rxjs/operators';
import { AdvancedTableState } from './advance-table-state.directive';
import { Subscription } from 'rxjs';
import { isEmpty } from 'lodash';

  export class AdvancedTableSearchComponentNotNull extends Error {
    constructor() {
      super('Advanced table search component cannot be empty at this context.');
    }
  }

  @Directive(
   {
     // tslint:disable-next-line: directive-selector
     selector: '[searchState]',
   },
  )
  // tslint:disable-next-line:directive-class-suffix
  export class AdvancedSearchState implements OnInit, OnDestroy {

    @Input() searchState?: string;
    private filterChangeSubscription?: Subscription;
    private tableInitSubscription?: Subscription;

    constructor(protected search: AdvancedTableSearchComponent,
        @Optional() protected store: Store<NgxTablesState>) {
    }
    ngOnInit(): void {
      if (this.store == null) {
        return;
      }
      if (this.search == null) {
          throw new AdvancedTableSearchComponentNotNull();
      }
      if (this.search.table) {
          // set auto load to false
          this.search.table.autoLoad = false;
          // handle filtering
          this.filterChangeSubscription = this.search.filterChange.subscribe((filter) => {
            this.onSearchFilter(filter);
          });
          // apply table state
          this.applyTableState();
          // handle table init
          this.tableInitSubscription = this.search.table.init.subscribe(() => {
            return this.onTableInit();
          });
      }
    }

    protected applyTableState() {
      if (this.search.table) {
        // handle table state (via directive)
        const tableState = new AdvancedTableState(this.search.table, this.store);
        tableState.tableState = this.searchState as string;
        tableState.ngOnInit();
      }
    }

    public onTableInit() {
      this.store.pipe(select(selectTable, { id: this.searchState as string}), first()).subscribe((state: any) => {
        // use order
        if (state) {
          if (state.order && state.order.length && this.search.table) {
            this.search.table.dataTable.order(state.order);
          }
          // use text search
          if (state.filter && state.filter.q && this.search.table) {
            this.search.text = state.filter.q;
            this.search.collapsed = true;
            return this.search.table.search(this.search.text);
          }
          // use predefined search
          if (state.filter && state.filter.s) {
            this.search.selectedList = this.search.userSearchList.find((item: { name: string }) => {
              return item.name === state.filter.s;
            });
            return this.search.search();
          }
          if (state.filter) {
            if (Object.keys(state.filter).length) {
              // set collapsed to false (advanced search)
              this.search.collapsed = false;
              this.search.filter = state.filter;
              return this.search.search();
            }
          }
          // ensure empty filter at this moment
          // because search() function triggers a store dispatch
          this.search.filter = {};
          return this.search.search();
        }
      });
    }

    private onSearchFilter(filter: any) {
      filter = isEmpty(filter) ? null : filter;
      this.store.dispatch(new AppendFilter(this.searchState as string, filter));
    }

    ngOnDestroy(): void {
      if (this.filterChangeSubscription) {
        this.filterChangeSubscription.unsubscribe();
      }
      if (this.tableInitSubscription) {
        this.tableInitSubscription.unsubscribe();
      }
    }
}
