/**
 *
 * @interface AdvancedAggregatorFilters An object that contains data used for filtering
 *
 * @property {Array<string>} filters A list of expressions to use for filtering
 * @property {string} text The search text used by the user to run against the declared search expression
 *
 */
export declare interface AdvancedAggregatorFilters {
  filters: string[];
  text: string;
}

/**
 *
 * @interface AdvancedAggregatorColumn A column in the advanced aggregator list
 *
 * @property {string} name The name of the column. It's used as it's key
 * @property {string} title The title that will be shown as column label
 * @property {string='text', 'boolean'} type The data type of the column value
 * @property {string} variant The a visualization variant of the column
 * @property {string} length The length of the list cell
 * @property {string} trueValue For booleans, the value that represent the true state
 * @property {boolean} hidden Whether the column should be shown on the UI
 *
 */
export declare interface AdvancedAggregatorColumn {
  name: string;
  property: string;
  title: string;
  type?: string;
  variant?: string;
  length?: string;
  trueValue?: string;
  hidden?: boolean;
}

/**
 *
 * @interface AdvancedAggregatorAction An action that can be used in a column
 *
 * @property {string} name The name of the action
 * @property {string='edit'} type The type of the action
 * @property {string} title The title that is shown on mouse hover
 * @property {string} identifier An identifier that will be emitted along with the row data
 * @property {string} icon The icon to show as identifier
 *
 */
export declare interface AdvancedAggregatorAction {
  name: string;
  type: string;
  title: string;
  identifier: string;
  icon: string;
}

/**
 *
 * @interface AdvancedAggregatorFunction An aggregator function that should return a value for each subgroup
 *
 * @property {string} name The name of the aggregator function
 * @property {string} label The label of the aggregator
 *
 */
export declare interface AdvancedAggregatorFunction {
  name: string;
  label: string;
}

/**
 *
 * @interface AdvancedAggregatorGroup The declaration of a property that can be used to group the data
 *
 * @property {string} name The name of the group. Should match the name of the column property
 * @property {string} property The property name
 * @property {string} label The label to be shown as the select option at the advanced search component
 * @property {string} title The title to show on top of the grouped list
 * @property {string} default Whether the column is used by default. If none, the first found is used
 *
 */
export declare interface AdvancedAggregatorGroup {
  name: string;
  property: string;
  label: string;
  title: string;
  default?: boolean;
}

/**
 *
 * @interface AdvancedAggregatorPostProcessor A declaration for a post processor function
 *
 * @property {string} name The name of the post processor. It will be used as identifier
 *
 */
export declare interface AdvancedAggregatorPostProcessor {
  name: string;
}

/**
 *
 * @interface AdvancedAggregatorConfig The configuration of the advanced aggregator list component
 *
 * @property {string} model The model which is going to be displayed
 * @property {string} query The query to use in order to fetch data
 * @property {string} title The Title of the list
 * @property {string} orderBy The property according which the list should be ordered
 * @property {Array<AdvancedAggregatorGroup>} groupProperties The list of the available grouping properties
 * @property {Array<AdvancedAggregatorFunction>} aggregator The list of functions that can summarize the data
 * @property {Array<AdvancedAggregatorAction>} actions The list of actions that are available for the list items
 * @property {Array<AdvancedAggregatorColumn>} columns The list of fields to be used from the target models
 *
 */
export declare interface AdvancedAggregatorConfig {
  model: string;
  query: string;
  title: string;
  orderBy: string;
  groupProperties: Array<AdvancedAggregatorGroup>;
  aggregators?: Array<AdvancedAggregatorFunction>;
  postProcessors?: Array<AdvancedAggregatorPostProcessor>;
  actions?: Array<AdvancedAggregatorAction>;
  columns: Array<AdvancedAggregatorColumn>;
}
