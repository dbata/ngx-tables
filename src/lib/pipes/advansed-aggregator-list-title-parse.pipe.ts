import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

/**
 *
 * 
 * @classdesc Given a title of an aggregator list, translates and evaluates the parameter if any
 *
 */

@Pipe({
  name: 'aggregatorListTitle'
})
export class ParseTitlePipe implements PipeTransform {

  constructor(
    private _translateService: TranslateService
  ) {}

  /**
   *
   * @override
   *
   * @param title The title of the list
   * @param key The current list key that is being used
   *
   */
  transform(title: string, key: string): any {
    if (!title) {
      return '';
    } else {
      if (key === undefined || key === null || key === 'undefined' || key === 'null') {
        key = this._translateService.instant('Tables.NotDefined');
      }
      return title.split(' ')
        .map((part) => this._translateService.instant(part))
        .map((part) => part.replace('${param}', key))
        .join(' ');
    }
  }
}
